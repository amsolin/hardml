import os
import time
import redis
import random
import string
import requests
import flask

secret_number = os.environ['SECRET_NUMBER_ARG']
app = flask.Flask(__name__)


@app.route('/return_secret_number')
def return_secret_number():
    return flask.jsonify({'secret_number': secret_number})


app.run(host='0.0.0.0', port=8000)
